var express = require("express");
var app = express();
app.use(express.static(__dirname +"/public"));
// app.use(function(req,res){
//     res.redirect("/error.html");
// });

app.set("port",process.argv[2]|| process.env.APP_PORT||3000);
app.listen(app.get("port"),function(){
    console.info("Application started on port %d", app.get("port"));
});

